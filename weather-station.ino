#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#include "Seeed_BME280.h"
#include <Wire.h>

BME280 bme280;

#define ssid ""
#define password ""

WiFiClient wifiClient;
int status = WL_IDLE_STATUS;

const byte WDIR = A0;
const byte WIND = 14;
const byte RAIN = 0;

volatile long lastWindIRQ = 0;
volatile byte windClicks = 0;
volatile long lastRain = 0;
volatile byte rainClicks = 0;

long loops = 0;
String payload = "";

void ICACHE_RAM_ATTR wspeedIRQ()
{
    if (millis() - lastWindIRQ > 10) // Ignore switch-bounce glitches less than 10ms (142MPH max reading) after the reed switch closes
    {
        lastWindIRQ = millis(); 
        windClicks++;     
    }
}

void ICACHE_RAM_ATTR rainIRQ()
// Count rain gauge bucket tips as they occur
// Activated by the magnet and reed switch in the rain gauge, attached to input D2
{
  if (millis() - lastRain > 100)
  {
    lastRain = millis();
    rainClicks ++;    
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  if(!bme280.init()){
    Serial.println("Device error!");
  }
  
  pinMode(WIND, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(WIND), wspeedIRQ, FALLING);  
  pinMode(RAIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RAIN), rainIRQ, FALLING);
  
  interrupts();

  WiFiOff();
}

void loop() { 
  if ( loops == 0 )
  {
  payload = "{ \"data\": [";
  }
  
  int windDirection = getWindDirection();
  float humidity = bme280.getHumidity();
  float tempC = bme280.getTemperature();
  float pressure = bme280.getPressure();

  String dataEntry = "";
  dataEntry += String(windDirection);
  dataEntry += "|";
  dataEntry += String(windClicks);
  dataEntry += "|";
  dataEntry += String(rainClicks);
  dataEntry += "|";
  dataEntry += String(humidity);
  dataEntry += "|";
  dataEntry += String(tempC);
  dataEntry += "|";
  dataEntry += String(pressure);

  if ( loops > 0 )
  {
    payload += ","; 
  }
  payload += "\"";
  payload += dataEntry;
  payload += "\"";
 
  windClicks = 0;
  rainClicks = 0;


  if (loops == 25)
  {
    WiFiOn();    
  }
  
  if (loops == 29)
  {
    payload += "] }";
    sendPayload(payload);
    loops = 0;
    WiFiOff();
  }
  else
  {
    loops++;
    delay(2000);
    Serial.println("loop " + String(loops));
  }
}

void WiFiOn() {
  Serial.println("Reconnecting");
  WiFi.forceSleepWake();
  delay( 1 );

  // Bring up the WiFi connection
  // initialize serial for ESP module
  //WiFi.persistent( false );
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
}


void WiFiOff() {
  Serial.println("diconnecting client and wifi");
  WiFi.disconnect( true );
  WiFi.mode( WIFI_OFF ); 
  WiFi.forceSleepBegin();
  delay( 1000 );
}

void sendPayload(String payload) {
if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;

    Serial.print("[HTTP] begin...\n");
    if (http.begin(wifiClient, "http://192.168.0.101:8090/weather")) {  // HTTP
      http.addHeader("Content-Type", "text/plain");

      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.POST(payload);

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }
  }

  
}


int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0;

  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;

  return(runningValue);
}

// read the wind direction sensor, return heading in degrees
int getWindDirection()
{
  unsigned int adc;

  adc = averageAnalogRead(WDIR); // get the current reading from the sensor

  if (adc < 125) return (113);
  if (adc < 152) return (68);
  if (adc < 180) return (90);
  if (adc < 225) return (158);
  if (adc < 310) return (135);
  if (adc < 380) return (203);
  if (adc < 440) return (180);
  if (adc < 580) return (23);
  if (adc < 635) return (45);
  if (adc < 765) return (248);
  if (adc < 795) return (225);
  if (adc < 855) return (338);
  if (adc < 920) return (0);
  if (adc < 950) return (293);
  if (adc < 995) return (315);
  if (adc < 1024) return (270);
  return (-1); // error, disconnected?
}
